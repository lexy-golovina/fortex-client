const webpack = require('webpack');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const InterpolateHtmlPlugin = require('interpolate-html-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = (env) => {
    return {
        entry: __dirname + '/src/index.js',
        module: {
            rules: [{
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env', "@babel/react", {
                        'plugins': ['@babel/plugin-proposal-class-properties']
                    }]
                }
            }, {
                test: /\.scss$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader"
                ]
            },
                {
                    test: /\.(png|jpe?g|gif|svg)$/,
                    use: [{
                        loader: 'file-loader',
                        options: {},
                    }],
                }]
        },
        devServer: {
            contentBase: __dirname + '.',
            hot: true,
            historyApiFallback: true,
            host: '0.0.0.0',
            disableHostCheck: true,
            port: process.env.PORT,
        },
        output: {
            filename: 'transformed.js',
            path: __dirname + '/build'
        },
        optimization: {
            minimizer: [new UglifyJsPlugin()],
            splitChunks: {
                chunks: 'all'
            }
        },
        plugins: [
            new CleanWebpackPlugin(),
            new HTMLWebpackPlugin({
                title: 'Fortex',
                template: __dirname + '/public/index.html',
                filename: 'index.html',
            }),
            new webpack.HotModuleReplacementPlugin(),
            new InterpolateHtmlPlugin({
                'PUBLIC_URL': 'public'
            }),
            new webpack.DefinePlugin({
                'process.env': {
                    'NODE_ENV': JSON.stringify(`${env}`)
                }
            })
        ]
    }
};