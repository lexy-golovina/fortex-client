FROM node:12.8.1-slim

RUN npm install -g pnpm
COPY package.json .
RUN pnpm install

COPY ./ ./

RUN pnpm run build-prod
CMD pnpm run start-prod
