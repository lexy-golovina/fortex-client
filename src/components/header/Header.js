import React, { useState } from 'react';
import HamburgerMenu from '../../images/hamburger-menu.png';
import './Header.scss';
import { Link } from "react-scroll";

export default (props) => {
    const [openMenu, setOpenMenu] = useState(props.isDesktop);

    const toggle = () => {
        setOpenMenu(prevState => !prevState);
    };

    return (
        <div className='header'>
            <img src={HamburgerMenu} className='hamburger-menu' onClick={toggle}/>
            {openMenu &&
            <div className="nav">
                <Link to="home" spy={true} smooth={true} duration={500} offset={-90} className="nav-item" activeClass="nav-item__active">Главная</Link>
                <Link to="shop" spy={true} smooth={true} duration={500} offset={-90} className="nav-item" activeClass="nav-item__active">Товары</Link>
                <Link to="about" spy={true} smooth={true} duration={500} offset={-60} className="nav-item" activeClass="nav-item__active">О нас</Link>
                <Link to="contacts" spy={true} smooth={true} duration={500} offset={-60} className="nav-item" activeClass="nav-item__active">Контакты</Link>
            </div>
            }
        </div>
    );
}