import * as React from "react/cjs/react.development";
import "./DefaultInput.scss";

const DefaultInput = props => {
    return (
        <div className="input input-group">
            {props.showIcon &&
            <img className={`glyphicon ${props.iconClass}`} src={props.icon}/>
            }
            {props.children}
        </div>
    );
};

export default DefaultInput;