import * as React from "react/cjs/react.development";

const DefaultButton = props => {
    return (
        <div>
            <div className={props.className}>
                {props.children}
                <span className={props.updateClass}>{props.updateMessage}</span>
            </div>
        </div>
    );
};

export default DefaultButton;