import React from "react";
import "./Root.scss";

const Root = props => {
    return (
        <div className="main">
            {props.children}
        </div>
    );
};

export default Root;