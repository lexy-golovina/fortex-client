import React from 'react';
import './PriceSort.scss';
import {connect} from "react-redux";
import {sortProductsByPrice} from "../../../actions/productActions";
import {SORT_BY_PRICE_ASC, SORT_BY_PRICE_DESC} from "../../../constants/generalConstants";

class PriceSort extends React.Component {

    state = {
        priceSortAsc: true,
        priceSortDesc: false
    };

    selectPriceAscSort = () => {
        this.setState({
            priceSortAsc: true,
            priceSortDesc: false
        });
        this.props.sortProductsByPrice(SORT_BY_PRICE_ASC);
    };

    selectPriceDescSort = () => {
        this.setState({
            priceSortAsc: false,
            priceSortDesc: true
        });
        this.props.sortProductsByPrice(SORT_BY_PRICE_DESC);
    };

    render() {
        const priceSortAsc = this.state.priceSortAsc;
        const priceSortDesc = this.state.priceSortDesc;
        return (
            <div>
               <span className="price-sort price-sort__header">Цена:</span>&nbsp;
               <span className={'price-sort ' + (priceSortAsc ? 'price-sort__option--active': 'price-sort__option')} onClick={this.selectPriceAscSort}>По возрастанию</span>
                &nbsp;|&nbsp;
               <span className={'price-sort ' + (priceSortDesc ? 'price-sort__option--active': 'price-sort__option')} onClick={this.selectPriceDescSort}>По убыванию</span>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        sortProductsByPrice: sortType => dispatch(sortProductsByPrice(sortType))
    }
};

export default connect(null, mapDispatchToProps)(PriceSort)