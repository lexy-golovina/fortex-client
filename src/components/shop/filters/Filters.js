import React from 'react';
import './Filters.scss';
import {DROP_DOWN_FILTER, RANGE_FILTER} from "../../../constants/generalConstants";
import DropDownFilter from "./DropDownFilter";
import RangeFilter from "./RangeFilter";
import {connect} from "react-redux";
import {fetchFiltersBegin, fetchFiltersFailure, fetchFiltersSuccess} from "../../../actions/filterActions";
import handleExceptions from "../../../utils/exceptionHandler";
import {API_URL} from "../../../config/config";

class Filters extends React.Component {

    fetchFilters() {
        return dispatch => {
            dispatch(fetchFiltersBegin());
            return fetch(`${API_URL}/filters`)
                .then(handleExceptions)
                .then(res => res.json())
                .then(json => {
                    dispatch(fetchFiltersSuccess(json));
                    return json;
                })
                .catch(error => dispatch(fetchFiltersFailure(error)));
        };
    }

    componentDidMount() {
        this.props.dispatch(this.fetchFilters());
    }

    renderFilters(filter, admin, product) {
        switch (filter.type) {
            case DROP_DOWN_FILTER: { return <DropDownFilter filter={filter} admin={admin} product={product}/> }
            case RANGE_FILTER: { return <RangeFilter filter={filter} admin={admin}/> }
        }
    }

    applyFilter(event, product) {
        event.preventDefault();

        const filterIds = Array.from(document.getElementsByClassName('filter__body-input'))
            .filter(i => i.checked)
            .map(i => i.value);

        fetch(`${API_URL}/products/${product.id}`, {
            method: "PATCH",
            body: new URLSearchParams({filterIds})
        })
    }

    render() {
        let { filters } = this.props;
        const { error, loading, admin, product } = this.props;

        if (admin) {
            filters = filters.filter(f => f.type === DROP_DOWN_FILTER)
        }

        if (error) {
            return <div>Error! {error.message}</div>;
        }

        if (loading) {
            return <div>Loading...</div>;
        }

        const filterRowClass = admin ? 'filter-row' : '';

        //TODO: unapply filters

        return (
            <div className="filter">
                <div className={filterRowClass}>
                {
                    filters.map(filter => <div key={filter.id}>{this.renderFilters(filter, admin, product)}</div>)
                }
                </div>
                {admin &&
                    <button className="filter__apply" onClick={(event) => this.applyFilter(event, product)}>Apply</button>
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    filters: state.filters.items,
    loading: state.filters.loading,
    error: state.filters.error,
});

export default connect(mapStateToProps)(Filters);