import React from 'react';
import './FilterHeader.scss';
import ArrowDown from '../../../../images/arrow-down.svg';
import ArrowRight from '../../../../images/arrow-right.svg';
import {connect} from "react-redux";
import {collapseFilter} from "../../../../actions/filterActions";

class FilterHeader extends React.Component {

    state = {
        arrowType: ArrowDown
    };

    toggle = () => {
        let arrowType = this.state.arrowType;
        switch (arrowType) {
            case ArrowDown: arrowType = ArrowRight; break;
            case ArrowRight: arrowType = ArrowDown; break;
        }

        this.setState({
            arrowType: arrowType
        });

        this.props.collapseFilter(this.props.filter.id)
    };

    render() {
        return (
            <div className="filter__header">
                <span className="fillers__header-name">{this.props.filter.name}</span>
                {!this.props.admin &&
                    <img className="filter__header-arrow" src={this.state.arrowType} onClick={this.toggle}/>
                }
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        collapseFilter: filterId => dispatch(collapseFilter(filterId))
    }
};

export default connect(null, mapDispatchToProps)(FilterHeader)