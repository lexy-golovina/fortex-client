import React from 'react';
import FilterHeader from "./common/FilterHeader";
import {filterProductsByPrice} from "../../../actions/productActions";
import {connect} from "react-redux";
import {DebounceInput} from 'react-debounce-input';

class RangeFilter extends React.Component {

    state = {
        min: this.props.filter.rangeFilterValue.min,
        max: this.props.filter.rangeFilterValue.max,
    };

    priceRangeChanged = (event) => {
        if (event.target.name === 'min') {
            this.setState({
                min: event.target.value
            })
        } else if (event.target.name === 'max') {
            this.setState({
                max: event.target.value
            })
        }

        this.props.filterProductsByPrice(this.state.min, this.state.max);
    };

    render() {
        const { filter, admin } = this.props;
        return (
            <div className="filter filter--range">
                <FilterHeader filter={filter} admin={admin}/>
                {filter.open &&
                    <div className="filter__body">
                        {/* TODO: add slider */}
                        {/* TODO: placeholder to contain min and max all the time */}
                        <DebounceInput debounceTimeout={300} name="min" placeholder={this.state.min} onChange={this.priceRangeChanged}/>
                        <DebounceInput debounceTimeout={300} name="max" placeholder={this.state.max} onChange={this.priceRangeChanged}/>
                    </div>
                }
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        filterProductsByPrice: (min, max) => dispatch(filterProductsByPrice(min, max))
    }
};

export default connect(null, mapDispatchToProps)(RangeFilter)