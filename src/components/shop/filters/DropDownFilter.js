import * as React from "react/cjs/react.development";
import FilterHeader from "./common/FilterHeader";
import {connect} from "react-redux";
import {filterProducts} from "../../../actions/productActions";
import "./Filters.scss";

class DropDownFilter extends React.Component {

    filterApplied(valueId, product) {
        if (product === undefined || product.productProperties === undefined) {
            return false;
        }

        return product.productProperties
            .map(prop => prop.id)
            .some(id => valueId === id)
    }

    renderFilterValues = (value, product) => (
        <div key={value.id}>
            <input className="filter__body-input" type="checkbox" defaultChecked={this.filterApplied(value.id, product)} value={value.id} onChange={() => this.props.filterProducts(value.id)}/>
            {value.name}
        </div>
    );

    render() {
        const { filter, product, admin } = this.props;
        return (
            <div className="filter filter--dropdown">
                <FilterHeader filter={filter} admin={admin}/>
                {filter.open &&
                    <div className="filter__body">
                        {filter.dropDownFilterValues.map(value => this.renderFilterValues(value, product))}
                    </div>
                }
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        filterProducts: filterId => dispatch(filterProducts(filterId))
    }
};

export default connect(null, mapDispatchToProps)(DropDownFilter)