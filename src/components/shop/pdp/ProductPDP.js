import React, {useState, useEffect} from "react";
import "./Product.scss";
import DefaultButton from "../../atoms/defaultButton/DefaultButton";
import handleExceptions from "../../../utils/exceptionHandler";
import ProductFilters from "../../admin/products/ProductFilters";
import {API_URL} from "../../../config/config";

const ProductPDP = props => {
    const [product, setProduct] = useState(undefined);
    const [editable, setEditable] = useState(false);
    const [updateClass, setUpdateClass] = useState('');
    const [updateMessage, setUpdateMessage] = useState('');

    const loadProduct = () => {
        fetch(`${API_URL}/products/${props.match.params}`)
            .then(handleExceptions)
            .then(res => res.json())
            .then(data => setProduct(data))
            .catch(err => {
                console.log("Server rejected response with: " + err);
            });
    };

    useEffect(() => {
        if (props.data) {
            const {product, editable} = props.data.location.state;
            setProduct(product);
            setEditable(editable);
        } else {
            loadProduct();
        }
    }, [props.data, props.match.params]);

    const handleChange = (event) => {
        product[event.target.name] = event.target.value;
        setProduct({product});
    };

    const editProduct = (event) => {
        event.preventDefault();

        if (!editable) {
            return;
        }

        fetch(`${API_URL}/products`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(this.state.product),
        })
            .then(handleExceptions)
            .then(() => {
                setUpdateClass('update-success');
                setUpdateMessage('Successfully updated!');
            })
            .catch(err => {
                console.log("Unsuccessful update error: " + err);
                setUpdateClass('update-error');
                setUpdateMessage('Update ended with error. See console logs');
            });
    };

    const editableClass = editable ? '--editable' : '';

    return (
        <React.Fragment>
            {product && <form onSubmit={editProduct}>
                <figure className="product-pdp">
                    <div className="product-pdp__img">
                        <img src={`data:image/jpeg;base64,${product.picture}`} alt='Picture'/>
                    </div>
                    <figcaption className="product-pdp__info product-pdp__info-group">
                        <input name="name"
                               className={`product-pdp__info product-pdp__info--name product-pdp__info ${editableClass}`}
                               defaultValue={product.name} onChange={handleChange}/>

                        <input name="price"
                               className={`product-pdp__info product-pdp__info--price product-pdp__info ${editableClass}`}
                               defaultValue={product.price} onChange={handleChange}/>

                        <textarea name="description"
                                  className={`product-pdp__info product-pdp__info--descr product-pdp__info ${editableClass}`}
                                  value={product.description} onChange={this.handleChange}/>
                    </figcaption>
                </figure>
                {editable &&
                <DefaultButton className="product-pdp__info--submit-group" updateClass={updateClass} updateMessage={updateMessage}>
                    <button className="admin product-pdp__info--submit-btn" type="submit">Update</button>
                </DefaultButton>
                }
            </form>
            }
            {editable && <ProductFilters product={product}/>}
        </React.Fragment>
    );
}

export default ProductPDP;