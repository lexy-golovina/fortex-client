import React from 'react';
import './Products.scss';
import ProductPLP from "./ProductPLP";
import {connect} from "react-redux";
import {
    fetchProductsBegin,
    fetchProductsFailure,
    fetchProductsSuccess
} from "../../../actions/productActions";
import handleExceptions from "../../../utils/exceptionHandler";
import {API_URL} from "../../../config/config";

class Products extends React.Component {

    fetchProducts = () => {
        return dispatch => {
            dispatch(fetchProductsBegin());
            return fetch(`${API_URL}/products`)
                .then(handleExceptions)
                .then(res => res.json())
                .then(json => {
                    dispatch(fetchProductsSuccess(json));
                    return json;
                })
                .catch(error => dispatch(fetchProductsFailure(error)));
        };
    };

    componentDidMount() {
        this.props.dispatch(this.fetchProducts());
    }

    filterProducts() {
        const {products, searchBy, filterBy, minPrice, maxPrice} = this.props;

        let filteredProducts = this.filterBySearch(searchBy, products);
        filteredProducts = this.filterByPrice(minPrice, maxPrice, filteredProducts);
        return this.filterByFilterValues(filterBy, filteredProducts);
    }

    filterByPrice(minPrice, maxPrice, filteredProducts) {
        if (minPrice !== '' && maxPrice !== '' && maxPrice >= minPrice) {
            return filteredProducts
                .filter(product => product.price >= minPrice && product.price <= maxPrice);
        }
        return filteredProducts;
    }

    filterByFilterValues(filterBy, filteredProducts) {
        if (filterBy.length !== 0) {
            return filteredProducts
                .filter(
                    product => product.productProperties
                        .map(prop => prop.id)
                        .some(id => filterBy.includes(id))
                );
        }
        return filteredProducts;
    }

    filterBySearch(searchBy, products) {
        if (searchBy !== '') {
            return products.filter(
                product => searchBy === '' || product.name.toLowerCase().includes(searchBy)
            )
        }
        return products;
    }

    render() {
        const { error, loading } = this.props;

        if (error) {
            return <div>Error! {error.message}</div>;
        }

        if (loading) {
            return <div>Loading...</div>;
        }

        return (
            <div className="products">
                {this.filterProducts().map(elem => <ProductPLP key={elem.id} product={elem} editable={this.props.editable} />)}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    products: state.products.items,
    loading: state.products.loading,
    error: state.products.error,
    searchBy: state.products.searchBy,
    filterBy: state.products.filterBy,
    minPrice: state.products.minPrice,
    maxPrice: state.products.maxPrice
});

export default connect(mapStateToProps)(Products);
