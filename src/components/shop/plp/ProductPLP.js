import React from "react";
import './Products.scss';
import {Redirect} from "react-router-dom";
import {deleteProduct} from "../../../actions/productActions";
import {connect} from "react-redux";
import DefaultButton from "../../atoms/defaultButton/DefaultButton";
import handleExceptions from "../../../utils/exceptionHandler";
import {API_URL} from "../../../config/config";

class ProductPLP extends React.Component {

    state = {
        product: this.props.product,
        editable: this.props.editable,
        redirect: false,
        updateClass: '',
        updateMessage: ''
    };

    redirect = () => {
        const { product, editable } = this.state;

        const admin = window.location.pathname.includes('/admin') ? '/admin' : '';

        return <Redirect to={{
            pathname: `${admin}/products/${product.id}`,
            state: {
                editable: editable,
                product: product
            }
        }}/>;
    };

    redirectToProduct = () => {
       this.setState({redirect: true})
    };

    deleteProduct = (event) => {
        event.preventDefault();
        const productId = this.state.product.id;

        fetch(`${API_URL}/products/${productId}`, {
            method: "DELETE"
        })
            .then(handleExceptions)
            .then(() => {
                this.props.deleteProduct(productId);
            })
            .catch(err => {
                console.log("Unsuccessful update error: " + err);
                this.setState({
                    updateClass: 'update-error',
                    updateMessage: 'Update ended with error. See console logs',
                });
            });
    };

    render() {
        const { product, editable, redirect } = this.state;
        return (
            <div className="product">
                {redirect && this.redirect()}
                <figure >
                    <img className="product__image" src={`data:image/jpeg;base64,${product.picture}`} onClick={this.redirectToProduct}/>
                    <figcaption>
                        <p className={"product-info product-info--name"}>{product.name}</p>
                        <p className={"product-info product-info--price"}>{product.price}</p>
                    </figcaption>
                </figure>

                { editable &&
                    <div className="product-info__modify">
                        <button className="product-info product-info__modify--edit" onClick={this.redirectToProduct}>
                            Edit
                        </button>
                        <DefaultButton updateClass={this.state.updateClass} updateMessage={this.state.updateMessage}>
                            <button className="product-info product-info__modify--delete" onClick={this.deleteProduct}>
                                Delete
                            </button>
                        </DefaultButton>
                    </div>
                }
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        deleteProduct: id => dispatch(deleteProduct(id))
    }
};

export default connect(null, mapDispatchToProps)(ProductPLP);