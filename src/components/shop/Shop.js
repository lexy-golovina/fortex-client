import React from "react";
import "./Shop.scss";
import Products from "./plp/Products";
import Filters from "./filters/Filters";
import SearchBar from "./search/SearchBar";
import PriceSort from "./sorts/PriceSort";

const Shop = () => {
    return (
        <div className="shop" id="shop">
            <aside className="shop-filters">
                <Filters/>
            </aside>
            <section className="shop-middle">
                <SearchBar/>
                <PriceSort/>
                <Products editable={false}/>
            </section>
        </div>
    );
}

export default Shop;