import React from 'react';
import './SearchBar.scss';
import SearchIcon from '../../../images/search-solid.png';
import DefaultInput from "../../atoms/defaultInput/DefaultInput";
import {connect} from "react-redux";
import {findProduct} from "../../../actions/productActions";
import {DebounceInput} from "react-debounce-input";

class SearchBar extends React.Component {

    search = (event) => {
        this.props.findProduct(event.target.value);
    };

    render() {
        return (
            <div className="search-bar">
                <DefaultInput showIcon={true} icon={SearchIcon} iconClass="glyphicon-search">
                    <DebounceInput debounceTimeout={300} type="text" placeholder="Поиск" onChange={this.search}/>
                </DefaultInput>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        findProduct: subString => dispatch(findProduct(subString))
    }
};

export default connect(null, mapDispatchToProps)(SearchBar)