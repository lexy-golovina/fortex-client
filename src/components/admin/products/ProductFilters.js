import React from 'react';
import Filters from "../../shop/filters/Filters";

const ProductFilter = () => {
    return (
        <div className="product-management__filters">
            <Filters admin={true} product={this.props.product}/>
        </div>
    );
};

export default ProductFilter;