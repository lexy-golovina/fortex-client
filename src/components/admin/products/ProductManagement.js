import React from "react";
import "./ProductManagement.scss";
import Products from "../../shop/plp/Products";
import SearchBar from "../../shop/search/SearchBar";
import {connect} from "react-redux";
import {addProduct} from "../../../actions/productActions";
import DefaultButton from "../../atoms/defaultButton/DefaultButton";
import handleExceptions from "../../../utils/exceptionHandler";
import {API_URL} from "../../../config/config";
import AdminTab from "../tab/AdminTab";

class ProductManagement extends React.Component{

    state = {
        product: {},
        updateClass: 'update-none',
        updateMessage: ''
    };

    enterFieldValue = (event) => {
        let product = {...this.state.product};
        product[event.target.name] = event.target.value;
        this.setState({product});
    };

    addProduct = (event) => {
        event.preventDefault();
        const form = event.target;

        const form_data = new FormData();

        for (let key in this.state.product) {
            form_data.append(key, this.state.product[key]);
        }

        fetch(`${API_URL}/products`, {
            method: "POST",
            headers: {
                'Accept': 'application/json'
            },
            body: form_data//JSON.stringify(this.state.product),
        })
            .then(handleExceptions)
            .then(res => res.json())
            .then(data => {
                this.props.addProduct(data);
                form.reset();
                this.setState({
                    updateClass: 'update-success',
                    updateMessage: 'Successfully updated!'
                });
            })
            .catch(err => {
                console.log("Unsuccessful update error: " + err);
                this.setState({
                    updateClass: 'update-error',
                    updateMessage: 'Update ended with error. See console logs',
                });
            });
    };

    onFileChangeHandler = (e) => {
        e.preventDefault();
        const product = this.state.product;
        product.picture = e.target.files[0];
        this.setState({product: product});
    };

    // TODO: fix file
    render() {
        return (
            <div className="product-management">
                <AdminTab/>
                <div className="product-management__main">
                    <form onSubmit={this.addProduct} className="product-management-form" encType="multipart/form-data">
                        <h2 className="product-management__header">New product</h2>
                        <input type="text" name="name" className="product-management-form__elem" placeholder="Product name" onChange={this.enterFieldValue}/>
                        <input type="text" name="price" placeholder="0.00" className="product-management-form__elem" onChange={this.enterFieldValue}/>
                        <textarea name="description" className="product-management-form__elem product-management-form__elem-text-area" placeholder="Description ..." onChange={this.enterFieldValue}/>
                        <input type="file" name="picture" className="product-management-form__elem" onChange={this.onFileChangeHandler}/>

                        <DefaultButton updateClass={this.state.updateClass} updateMessage={this.state.updateMessage}>
                            <button type="submit" className="product-management-form__elem product-management-form__elem-submit">Add</button>
                        </DefaultButton>
                    </form>

                    <SearchBar />
                    <Products editable={true} />
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addProduct: product => dispatch(addProduct(product))
    }
};

export default connect(null, mapDispatchToProps)(ProductManagement)