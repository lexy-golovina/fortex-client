import React from "react";
import "./AdminMain.scss";

const AdminMain = props => {
    return (
        <div className="admin">
            {props.children}
        </div>
    );
};

export default AdminMain;