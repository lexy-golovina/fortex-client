import React from "react";
import "./FilterManagement.scss";
import DefaultButton from "../../atoms/defaultButton/DefaultButton";
import handleExceptions from "../../../utils/exceptionHandler";
import PlusIcon from "../../../images/plus-sign.png";
import {DROP_DOWN_FILTER, NONE, RANGE_FILTER} from "../../../constants/generalConstants";
import connect from "react-redux/es/connect/connect";
import {addFilter} from "../../../actions/filterActions";
import CrossIcon from "../../../images/delete.png";
import {API_URL} from "../../../config/config";

class NewFilter extends React.Component {

    state = {
        filter: {
            dropDownFilterValues: [],
            rangeFilterValue: {}
        },
        updateClass: '',
        updateMessage: ''
    };

    handleSubmit = (event) => {
        event.preventDefault();

        const filter = this.state.filter;
        let dropDownFilterValues = [];

        //TODO: refactor

        const dropDownInputs = document.getElementsByClassName("filter-management--add")[0]
            .getElementsByClassName("filter-management__item--body-input--drop");
        for (let i = 0; i < dropDownInputs.length; ++i) {
            dropDownFilterValues.push({id: '', name: dropDownInputs[i].value});
        }

        filter.dropDownFilterValues = dropDownFilterValues;
        this.setState(filter);

        fetch(`${API_URL}/filters`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(filter)
        })
            .then(handleExceptions)
            .then(res => res.json())
            .then(data => {
                this.props.addFilter(data);
                this.setState({
                    updateClass: 'update-success',
                    updateMessage: 'Successfully added!'
                })
            })
            .catch(err => {
                console.error(err);
                this.setState({
                    updateClass: 'update-error',
                    updateMessage: 'Could not add a filter. See console logs',
                });
            });
    };

    renderRange = () => (
        <p className="filter-management__item--options">
            <span className="filter-management__item--name">Range:</span>
            <span className="filter-management__item--body">
                <span className="filter-management__item--body-range">
                    <span className="filter-management__item--body-range--name">min</span>
                    <input name="min"
                           onChange={this.updateRange}
                           className="filter-management__item--body-input"/>
                </span>
                <span className="filter-management__item--body-range">
                    <span className="filter-management__item--body-range--name">max</span>
                    <input name="max"
                           onChange={this.updateRange}
                           className="filter-management__item--body-input"/>
                </span>
            </span>
        </p>
    );

    handleRemoveOption = (event) => {
        event.preventDefault();
        event.target.parentNode.remove();
    };

    renderOptions = () => (
        <div>
            <p className="filter-management__item--options">
                <span className="filter-management__item--name">Options:</span>
                <span className="filter-management__item--body">
                     <span className="filter-management__item--body-input-grp">
                         <input className="filter-management__item--body-input filter-management__item--body-input--drop" name="dropDownFilterValues"/>
                         <img className="filter-management__item--body-input--delete-img" src={CrossIcon} onClick={this.handleRemoveOption}/>
                     </span>
                     {this.state.filter.dropDownFilterValues.map(value => (
                         <span className="filter-management__item--body-input-grp">
                             <input className="filter-management__item--body-input filter-management__item--body-input--drop"
                               name="dropDownFilterValues"/>

                             <img className="filter-management__item--body-input--delete-img" src={CrossIcon} onClick={this.handleRemoveOption}/>
                         </span>
                         )
                     )}
                </span>
            </p>
            <img src={PlusIcon} className="filter-management__item--options-add" onClick={this.addOptionInput}/>
        </div>
    );

    updateRange = (event) => {
        const filter = {...this.state.filter};
        filter.rangeFilterValue[event.target.name] = event.target.value;
        this.setState(filter);
    };

    addOptionInput = () => {
        const filter = {...this.state.filter};
        filter.dropDownFilterValues.push({id: "", name: ""});
        this.setState(filter);
    };

    updateFilterField = (event) => {
        const filter = this.state.filter;
        filter[event.target.name] = event.target.value;
        this.setState(filter);
    };

    render() {
        const filter = this.state.filter;
        return (
            <div className="filter-management__item">
                <h2 className="filter-management__item-header">New filter</h2>
                <form onSubmit={this.handleSubmit}>
                    <p>
                        <span className="filter-management__item--name">Filter Name:</span>
                        <input name="name" className="filter-management__item--body-input"
                               defaultValue={filter.name === undefined ? '' : filter.name}
                               onChange={this.updateFilterField}/>
                        <span className="filter-management__item--name">Filter Type:</span>&nbsp;
                        <select name="type" className="filter-management__item--body-input"
                                defaultValue={filter.name === undefined ? NONE : filter.type}
                                onChange={this.updateFilterField}>
                            <option value={NONE} disabled>Filter type</option>
                            <option value={DROP_DOWN_FILTER}>{DROP_DOWN_FILTER}</option>
                            <option value={RANGE_FILTER}>{RANGE_FILTER}</option>
                        </select>
                    </p>

                    {(() => {
                        switch (this.state.filter.type) {
                            case DROP_DOWN_FILTER:
                                return this.renderOptions();
                            case RANGE_FILTER:
                                return this.renderRange();
                        }
                    })()}

                    <DefaultButton updateClass={this.state.updateClass} updateMessage={this.state.updateMessage}>
                        <button className="filter-management__item--submit">Add</button>
                    </DefaultButton>
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addFilter: filter => dispatch(addFilter(filter))
    }
};

export default connect(null, mapDispatchToProps)(NewFilter)