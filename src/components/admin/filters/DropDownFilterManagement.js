import React from "react";
import "./FilterManagement.scss";
import DefaultButton from "../../atoms/defaultButton/DefaultButton";
import handleExceptions from "../../../utils/exceptionHandler";
import PlusIcon from "../../../images/plus-sign.png";
import {deleteFilter} from "../../../actions/filterActions";
import connect from "react-redux/es/connect/connect";
import CrossIcon from "../../../images/delete.png";
import {API_URL} from "../../../config/config";

class DropDownFilterManagement extends React.Component {

    state = {
        filter: this.props.filter,
        updateClass: '',
        updateMessage: ''
    };

    updateFilterValue = (event, filterId) => {
        let filter = {...this.state.filter};
        const filterElement = filter[event.target.name]
            .filter(filterValue => filterValue.id === filterId)[0];

        filterElement.name = event.target.value;
        this.setState({filter});
    };

    updateValue = (event) => {
        const filter = {...this.state.filter};
        filter[event.target.name] = event.target.value;
        this.setState({filter});
    };

    handleSubmit = (event) => {
        event.preventDefault();

        const filter = this.state.filter;
        fetch(`${API_URL}/filters`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(filter)
        })
            .then(handleExceptions)
            .then(res => res.json())
            .then(data => {
                for (let i = 0; i < filter.dropDownFilterValues.length; ++i) {
                    if (filter.dropDownFilterValues[i].id === "") {
                        filter.dropDownFilterValues[i].id = data.dropDownFilterValues[i].id;
                    }
                }

                this.setState({
                    filter: filter,
                    updateClass: 'update-success',
                    updateMessage: 'Successfully updated!'
                })
            })
            .catch(err => {
                console.error(err);
                this.setState({
                    updateClass: 'update-error',
                    updateMessage: 'Update ended with error. See console logs',
                });
            });
    };

    addOptionInput = () => {
        const filter = {...this.state.filter};
        filter.dropDownFilterValues.push({id: "", name: ""});
        this.setState(filter);
    };

    handleRemove = (event) => {
        event.preventDefault();

        const filterId = this.state.filter.id;
        fetch(`${API_URL}/filters/${filterId}`, {
            method: "DELETE",
        })
            .then(handleExceptions)
            .then(data => this.props.deleteFilter(filterId))
            .catch(err => console.log("Error while trying to delete a filter"));
    };

    handleRemoveOption = (event, valueId) => {
        event.preventDefault();
        event.target.parentNode.remove();

        fetch(`${API_URL}/filters/value/${valueId}`, {
            method: "DELETE",
        })
            .then(handleExceptions)
            .catch(err => console.log("Error while trying to delete a filter value"));
    };

    render() {
        const filter = this.state.filter;
        return (
            <form onSubmit={this.handleSubmit}>
                <p>
                    <span className="filter-management__item--name">Filter Name:</span>
                    <input name="name" className="filter-management__item--body-input" defaultValue={filter.name} onChange={this.updateValue}/>
                </p>
                <p className="filter-management__item--options">
                    <span className="filter-management__item--name">Options:</span>
                    <span className="filter-management__item--body">
                        {filter.dropDownFilterValues.map(value => (
                            <span className="filter-management__item--body-input-grp">
                                <input key={value.id} className="filter-management__item--body-input"
                                       name="dropDownFilterValues" onChange={(event) => this.updateFilterValue(event, value.id)}
                                       defaultValue={value.name}/>
                                <img className="filter-management__item--body-input--delete-img" src={CrossIcon} onClick={(event) => this.handleRemoveOption(event, value.id)}/>
                            </span>
                        ))}
                    </span>
                </p>
                <img src={PlusIcon} className="filter-management__item--options-add" onClick={this.addOptionInput}/>
                <DefaultButton className="filter-management__item--options-add-sbm" updateClass={this.state.updateClass} updateMessage={this.state.updateMessage}>
                    <button className="filter-management__item--submit">Update</button>
                </DefaultButton>
                <button className="filter-management__item--remove" onClick={this.handleRemove}>Remove</button>
            </form>
        );
    }
}


const mapDispatchToProps = dispatch => {
    return {
        deleteFilter: filterId => dispatch(deleteFilter(filterId))
    }
};

export default connect(null, mapDispatchToProps)(DropDownFilterManagement);