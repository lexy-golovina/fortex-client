import React from "react";
import "./FilterManagement.scss";
import {DROP_DOWN_FILTER, RANGE_FILTER} from "../../../constants/generalConstants";
import handleExceptions from "../../../utils/exceptionHandler";
import DropDownFilterManagement from "./DropDownFilterManagement";
import RangeFilterManagement from "./RangeFilterManagement";
import NewFilter from "./NewFilter";
import connect from "react-redux/es/connect/connect";
import {fetchFiltersBegin, fetchFiltersFailure, fetchFiltersSuccess} from "../../../actions/filterActions";
import {API_URL} from "../../../config/config";
import AdminTab from "../tab/AdminTab";

class FilterManagement extends React.Component {

    fetchFilters = () => {
        return dispatch => {
            dispatch(fetchFiltersBegin());
            return fetch(`${API_URL}/filters`)
                .then(handleExceptions)
                .then(res => res.json())
                .then(json => {
                    dispatch(fetchFiltersSuccess(json));
                    return json;
                })
                .catch(error => dispatch(fetchFiltersFailure(error)));
        };
    };

    componentDidMount() {
        this.props.dispatch(this.fetchFilters());
    }

    renderFilter = (filter) => (
        <div key={filter.id} className="filter-management__item">
            {(() => {
                switch (filter.type) {
                    case DROP_DOWN_FILTER: {
                        return <DropDownFilterManagement filter={filter} />;
                    }
                    case RANGE_FILTER: {
                        return <RangeFilterManagement filter={filter} />;
                    }
                }
            })()}
        </div>
    );

    render() {
        return (
            <div className="filter-management">
                <AdminTab/>
                <div className="filter-management__main">
                    <div className="filter-management--add">
                        <NewFilter />
                    </div>
                    <div className="filter-management--update">
                        {this.props.filters.map(this.renderFilter)}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    filters: state.filters.items,
});

export default connect(mapStateToProps)(FilterManagement);