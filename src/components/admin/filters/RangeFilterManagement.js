import React from "react";
import "./FilterManagement.scss";
import DefaultButton from "../../atoms/defaultButton/DefaultButton";
import handleExceptions from "../../../utils/exceptionHandler";
import {deleteFilter} from "../../../actions/filterActions";
import connect from "react-redux/es/connect/connect";
import {API_URL} from "../../../config/config";

class RangeFilterManagement extends React.Component {

    state = {
        filter: this.props.filter,
        updateClass: '',
        updateMessage: ''
    };

    updateFilterValue = (event, filterType) => {
        let filter = {...this.state.filter};
        filter[event.target.name][filterType] = event.target.value;
        this.setState({filter});
    };

    updateValue = (event) => {
        let filter = {...this.state.filter};
        filter[event.target.name] = event.target.value;
        this.setState({filter});
    };

    handleSubmit = (event) => {
        event.preventDefault();

        fetch(`${API_URL}/filters`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(this.state.filter)
        })
            .then(handleExceptions)
            .then(data => {
                this.setState({
                    updateClass: 'update-success',
                    updateMessage: 'Successfully updated!'
                })
            })
            .catch(err => {
                console.error(err);
                this.setState({
                    updateClass: 'update-error',
                    updateMessage: 'Update ended with error. See console logs',
                });
            });
    };

    handleRemove = (event) => {
        event.preventDefault();

        const filterId = this.state.filter.id;
        fetch(`${API_URL}/filters/${filterId}`, {
            method: "DELETE",
        })
            .then(handleExceptions)
            .then(data => this.props.deleteFilter(filterId))
            .catch(err => console.log("Error"));
    };

    render() {
        const filter = this.state.filter;
        return (
            <form onSubmit={this.handleSubmit}>
                <p>
                    <span className="filter-management__item--name">Filter Name:</span>
                    <input name="name" className="filter-management__item--body-input" defaultValue={filter.name} onChange={this.updateValue}/>
                </p>
                <p className="filter-management__item--options">
                    <span className="filter-management__item--name">Range:</span>
                    <span className="filter-management__item--body">
                        <span className="filter-management__item--body-range">
                            <span className="filter-management__item--body-range--name">min</span>
                            <input name="rangeFilterValue" className="filter-management__item--body-input"
                                   defaultValue={filter.rangeFilterValue.min} onChange={(event) => this.updateFilterValue(event, "min")}/>
                        </span>
                        <span className="filter-management__item--body-range">
                            <span className="filter-management__item--body-range--name">max</span>
                            <input name="rangeFilterValue" className="filter-management__item--body-input"
                                   defaultValue={filter.rangeFilterValue.max} onChange={(event) => this.updateFilterValue(event, "max")}/>
                        </span>
                    </span>
                </p>
                <DefaultButton className="filter-management__item--options-add-sbm" updateClass={this.state.updateClass} updateMessage={this.state.updateMessage}>
                    <button className="filter-management__item--submit">Update</button>
                </DefaultButton>
                <button className="filter-management__item--remove" onClick={this.handleRemove}>Remove</button>
            </form>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        deleteFilter: filterId => dispatch(deleteFilter(filterId))
    }
};

export default connect(null, mapDispatchToProps)(RangeFilterManagement);