import React, {useState, useEffect} from "react";
import "./StoreManagement.scss";
import DefaultButton from "../../atoms/defaultButton/DefaultButton";
import handleExceptions from "../../../utils/exceptionHandler";
import {API_URL} from "../../../config/config";
import AdminTab from "../tab/AdminTab";

const StoreManagement = () => {
    const [storeInfo, setStoreInfo] = useState({});
    const [updateClass, setUpdateClass] = useState('update-none');
    const [updateMessage, setUpdateMessage] = useState('');

    useEffect(() => {
        fetch(`${API_URL}/storeInfo`)
            .then(handleExceptions)
            .then(res => res.json())
            .then(data => {
                data.description = data.description.replace(/\s{2,}/g, '');
                setStoreInfo(data);
            })
            .catch(err => {
                console.log("Server rejected response with: " + err);
            });
    }, []);

    const updateStore = event => {
        event.preventDefault();

        fetch(`${API_URL}/storeInfo`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(storeInfo),
        })
            .then(handleExceptions)
            .then(() => {
                setUpdateClass('update-success');
                setUpdateMessage('Successfully updated!');
            })
            .catch(err => {
                setUpdateClass('update-error');
                setUpdateMessage('Update ended with error. See console logs');
                console.log("Unsuccessful update error: " + err);
            });
    };

    const handleChange = event => {
        storeInfo[event.target.name] = event.target.value;
        setStoreInfo(storeInfo);
    };

    const address = storeInfo.address;

    return (
        <div className="store-management">
            <AdminTab/>
            <div className="store-management__main">
                <h2 className="store-management__header">Store information</h2>

                <form onSubmit={updateStore} className="store-management-form">
                    <input type="text" name="name" className="store-management-form__elem"
                           defaultValue={storeInfo.name} onChange={handleChange}/>

                    <textarea className="store-management-form__elem store-management-form__elem-text-area"
                              name="description"
                              value={storeInfo.description} onChange={handleChange}/>

                    <input type="file" className="store-management-form__elem"/>

                    <input type="text" name="phone" className="store-management-form__elem"
                           defaultValue={storeInfo.phone} onChange={handleChange}/>

                    <input id="streetName" type="text" name="address.streetName"
                           className="store-management-form__elem"
                           defaultValue={!address ? '' : address.streetName}
                           onChange={handleChange}/>

                    <input id="streetNumber" type="text" name="address.streetNumber"
                           className="store-management-form__elem"
                           defaultValue={!address ? '' : address.streetNumber}
                           onChange={handleChange}/>

                    <input id="zip" type="text" name="address.zip" className="store-management-form__elem"
                           defaultValue={!address ? '' : address.zip}
                           onChange={handleChange}/>

                    <input id="city" type="text" name="address.city"
                           className="store-management-form__elem"
                           defaultValue={!address ? '' : address.city}
                           onChange={handleChange}/>

                    <input id="country" type="text" name="address.country"
                           className="store-management-form__elem"
                           defaultValue={!address ? '' : address.country}
                           onChange={handleChange}/>

                    <DefaultButton updateClass={updateClass} updateMessage={updateMessage}>
                        <button type="submit" className="store-management-form__elem store-management-form__elem-submit">
                            Update
                        </button>
                    </DefaultButton>
                </form>
            </div>
        </div>
    );
}

export default StoreManagement;