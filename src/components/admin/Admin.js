import React from "react";
import "./Admin.scss";
import ProductManagement from "./products/ProductManagement";
import StoreManagement from "./store_info/StoreManagement";
import {BrowserRouter, Route} from "react-router-dom";
import FilterManagement from "./filters/FilterManagement";
import AdminAuth from "./auth/AdminAuth";
import AuthenticatedComponent from "./auth/AuthenticatedComponent";
import logout from "./auth/logout";
import ProductPDP from "../shop/pdp/ProductPDP";

const Admin = () => {
    return (
        <BrowserRouter>
            <Route exact path="/admin" component={AdminAuth}/>
            <Route exact path="/admin/products"
                   component={() => <AuthenticatedComponent><ProductManagement/></AuthenticatedComponent>}/>
            <Route path="/admin/filters"
                   component={() => <AuthenticatedComponent><FilterManagement/></AuthenticatedComponent>}/>
            <Route path="/admin/products/:id"
                   component={(props) => <AuthenticatedComponent><ProductPDP data={props}/></AuthenticatedComponent>}/>
            <Route path="/admin/store"
                   component={() => <AuthenticatedComponent><StoreManagement/></AuthenticatedComponent>}/>
            <Route path="/admin/logout" component={() => logout()}/>
        </BrowserRouter>
    );
}

export default Admin;