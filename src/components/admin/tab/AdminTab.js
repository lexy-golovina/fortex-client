import React from "react";
import "./AdminTab.scss";
import {NavLink} from "react-router-dom";

const AdminTab = () => {
    return (
        <div className="admin-tab">
            <NavLink to="/admin/products" exact className="admin-tab__nav-item"
                     activeClassName="admin-tab__nav-item--active">Products</NavLink>
            <NavLink to="/admin/filters" className="admin-tab__nav-item"
                     activeClassName="admin-tab__nav-item--active">Filters</NavLink>
            <NavLink to="/admin/store" className="admin-tab__nav-item"
                     activeClassName="admin-tab__nav-item--active">Store info</NavLink>
            <NavLink to="/admin/logout" className="admin-tab__nav-item"
                     activeClassName="admin-tab__nav-item--active">Log out</NavLink>
        </div>
    );
}

export default AdminTab;