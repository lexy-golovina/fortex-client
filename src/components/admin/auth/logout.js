import {API_URL} from "../../../config/config";
import handleExceptions from "../../../utils/exceptionHandler";

export default function logout() {
    fetch(`${API_URL}/logout`)
        .then(handleExceptions)
        .then(data => {
            localStorage.removeItem("hasAuth");  //TODO: redo to store token
            window.location = "/admin";
        })
        .catch(err => {
            console.log("Server rejected response with: " + err);
        });
}