import React, {useState} from "react";
import "./AdminAuth.scss";
import DefaultButton from "../../atoms/defaultButton/DefaultButton";
import handleExceptions from "../../../utils/exceptionHandler";
import {API_URL} from "../../../config/config";

const AdminAuth = () => {
    const [forbidden, setForbidden] = useState('');

    const logIn = event => {
        event.preventDefault();

        const formData = new FormData(event.target);
        fetch(`${API_URL}/login`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            credentials: 'include',
            body: new URLSearchParams(formData)
        })
            .then(handleExceptions)
            .then(response => {
                if (response.status === 200) {
                    localStorage.setItem("hasAuth", "true");
                    window.location = "/admin/products";
                } else {
                    localStorage.setItem("hasAuth", "false");
                    setForbidden('Forbidden!');
                }
            })
            .catch(e => console.error(e))
    };

    return (
        <div className="admin-auth">
            <div className="admin-auth__form">
                <h2>Enter credentials</h2>
                <form onSubmit={logIn}>
                    <input type="text" name="username" className="admin-auth__form-input" placeholder="Username"/>
                    <input type="password" name="password" className="admin-auth__form-input" placeholder="Password"/>
                    <DefaultButton updateClass="update-error" updateMessage={forbidden}>
                        <button type="submit" className="admin-auth__form-submit">Log in</button>
                    </DefaultButton>
                </form>
            </div>
        </div>
    );
}

export default AdminAuth;