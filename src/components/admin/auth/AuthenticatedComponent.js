import React from 'react'
import AdminAuth from "./AdminAuth";

const AuthenticatedComponent = props => {
    return (localStorage.getItem("hasAuth")
            ? <div>{props.children}</div>
            : <AdminAuth/>
    )
};

export default AuthenticatedComponent;