import React from "react";
import "./Home.scss";

const Home = () => {
    return (
        <div className="home" id="home">
            <img src={require("../../images/homePage.jpg")} alt='Home'/>
        </div>
    )
};

export default Home;