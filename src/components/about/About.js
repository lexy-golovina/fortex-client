import React, {useState, useEffect} from "react";
import "./About.scss";
import handleExceptions from "../../utils/exceptionHandler";
import {API_URL} from "../../config/config";

const About = () => {
    const [storeInfo, setStoreInfo] = useState({});

    useEffect(() => {
        fetch(`${API_URL}/storeInfo`)
            .then(handleExceptions)
            .then(res => res.json())
            .then(data => setStoreInfo(data))
            .catch(err => {
                console.log("Server rejected response with: " + err);
            });
    }, []);

    return (
        <div className="about" id="about">
            <h1 className="about-name">{storeInfo.name}</h1>
            <p className="about-text">{storeInfo.description}</p>
        </div>
    );
};

export default About;