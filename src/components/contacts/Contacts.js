import React from "react";
import "./Contacts.scss";
import MapContainer from "./MapContainer";
import handleExceptions from "../../utils/exceptionHandler";
import {API_URL} from "../../config/config";

export default class Contacts extends React.Component {

    state = {
        storeInfo: {}
    };

    componentDidMount() {
        fetch(`${API_URL}/storeInfo`)
            .then(handleExceptions)
            .then(res => res.json())
            .then(data => this.setState({storeInfo: data}))
            .catch(err => {
                console.log("Server rejected response with: " + err);
            });
        //TODO: change all catch blocks to be more informative
    }

    render() {
        const storeInfo = this.state.storeInfo;
        const address = storeInfo.address;
        const googleMap = storeInfo.googleMap;
        return (
            <div className="location" id="contacts">
                <div className="location-address">
                    <h2 className="location-address__store-name">{storeInfo.name}</h2>
                    { address !== undefined &&
                         <p className="location-address__line">{address.streetName} {address.streetNumber}, {address.city}, {address.country}</p>
                    }
                    <p className="location-address__line">{storeInfo.phone}</p>
                </div>
                <div className="location-maps">
                    {googleMap !== undefined &&
                        <MapContainer googleMap={googleMap}/>
                    }
                </div>
            </div>
        );
    }
}