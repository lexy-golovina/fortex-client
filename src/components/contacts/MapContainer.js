import React from 'react';
import GoogleMapReact from 'google-map-react';

const MarkerComponent = () => <div className="marker"></div>;

export default class MapContainer extends React.Component {

    center = {
        lat: this.props.googleMap.latitude,
        lng: this.props.googleMap.longitude
    };

    render() {
        return (
            <div style={{ height: '100vh', width: '100%' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: this.props.googleMap.googleKey }}
                    defaultCenter={this.center}
                    defaultZoom={17}>

                    <MarkerComponent
                        lat={this.center.lat}
                        lng={this.center.lng}
                    />
                </GoogleMapReact>
            </div>
        );
    }
}
