import React from "react";
import Shop from "../shop/Shop";
import About from "../about/About";
import Contacts from "../contacts/Contacts";
import Header from "../header/Header";
import Home from "../home/Home";

const Main = () => {
    return (
        <React.Fragment>
            <Header isDesktop={window.innerWidth >= 450}/>
            <Home/>
            <Shop/>
            <About/>
            <Contacts/>
        </React.Fragment>
    )
};

export default Main;