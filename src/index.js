import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import * as serviceWorker from './serviceWorker';
import Root from "./components/root/Root";
import {BrowserRouter, Route} from "react-router-dom";
import ProductPDP from "./components/shop/pdp/ProductPDP";
import Main from "./components/main/Main";
import {Provider} from "react-redux";
import store from "./store/index";
import Admin from "./components/admin/Admin";

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Root>
                <Route exact path="/" component={Main}/>
                <Route exact path="/home" component={Main}/>
                <Route exact path="/shop" component={Main}/>
                <Route exact path="/about" component={Main}/>
                <Route exact path="/contacts" component={Main}/>
                <Route path="/products/:id" component={ProductPDP}/>
                <Route path="/admin" component={Admin}/>
            </Root>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
