export default function handleExceptions(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}