import {
    ADD_FILTER,
    COLLAPSE_FILTER, DELETE_FILTER,
    FETCH_FILTERS_BEGIN,
    FETCH_FILTERS_FAILURE,
    FETCH_FILTERS_SUCCESS
} from "../constants/actionTypes";


export const fetchFiltersBegin = () => ({
    type: FETCH_FILTERS_BEGIN
});

export const fetchFiltersSuccess = filters => ({
    type: FETCH_FILTERS_SUCCESS,
    payload: { filters }
});

export const fetchFiltersFailure = error => ({
    type: FETCH_FILTERS_FAILURE,
    payload: { error: error }
});

export const collapseFilter = id => ({
    type: COLLAPSE_FILTER,
    payload: {id}
});

export const addFilter = filter => ({
    type: ADD_FILTER,
    payload: {filter}
});

export const deleteFilter = filterId => ({
    type: DELETE_FILTER,
    payload: {filterId}
});

