import {
    ADD_PRODUCT, DELETE_PRODUCT,
    FETCH_PRODUCTS_BEGIN,
    FETCH_PRODUCTS_FAILURE,
    FETCH_PRODUCTS_SUCCESS, FILTER_PRODUCTS, FILTER_PRODUCTS_BY_PRICE, FIND_PRODUCT, SORT_BY_PRICE
} from "../constants/actionTypes";


export const fetchProductsBegin = () => ({
    type: FETCH_PRODUCTS_BEGIN
});

export const fetchProductsSuccess = products => ({
    type: FETCH_PRODUCTS_SUCCESS,
    payload: { products }
});

export const fetchProductsFailure = error => ({
    type: FETCH_PRODUCTS_FAILURE,
    payload: { error }
});

export const addProduct = product => ({
    type: ADD_PRODUCT,
    payload: {product}
});

export const deleteProduct = id => ({
    type: DELETE_PRODUCT,
    payload: {id}
});

export const sortProductsByPrice = sortType => ({
    type: SORT_BY_PRICE,
    payload: {sortType}
});

export const findProduct = subString => ({
    type: FIND_PRODUCT,
    payload: {subString}
});

export const filterProducts = filterId => ({
   type: FILTER_PRODUCTS,
   payload: {filterId}
});

export const filterProductsByPrice = (min, max) => ({
   type: FILTER_PRODUCTS_BY_PRICE,
   payload: {
       min: min,
       max: max
   }
});