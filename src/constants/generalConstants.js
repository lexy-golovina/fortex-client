export const SORT_BY_PRICE_ASC = 'asc';
export const SORT_BY_PRICE_DESC = 'desc';
export const DROP_DOWN_FILTER = 'DROP_DOWN';
export const RANGE_FILTER = 'RANGE';
export const NONE = 'none';