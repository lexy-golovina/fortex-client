import {
    ADD_PRODUCT, DELETE_PRODUCT,
    FETCH_PRODUCTS_BEGIN,
    FETCH_PRODUCTS_FAILURE,
    FETCH_PRODUCTS_SUCCESS, FILTER_PRODUCTS, FILTER_PRODUCTS_BY_PRICE, FIND_PRODUCT, SORT_BY_PRICE
} from "../constants/actionTypes";
import {SORT_BY_PRICE_ASC, SORT_BY_PRICE_DESC} from "../constants/generalConstants";


const initialState = {
    items: [],
    loading: false,
    error: null,
    searchBy: '',
    filterBy: [],
    minPrice: '',
    maxPrice: ''
};

function startLoadingProducts(state) {
    return {
        ...state,
        loading: true,
        error: null
    };
}

function getLoadedProducts(state, action) {
    return {
        ...state,
        loading: false,
        items: action.payload.products
    };
}

function getErrorFromLoadingProducts(state, action) {
    return {
        ...state,
        loading: false,
        error: action.payload.error,
        items: []
    };
}

function addProduct(state, action) {
    return {
        ...state,
        items: state.items.concat(action.payload.product)
    };
}

function deleteProduct(state, action) {
    return {
        ...state,
        items: state.items.filter(product => product.id !== action.payload.id)
    };
}

function sort(p1, p2) {
    if (p1.price > p2.price) {
        return 1;
    }
    if (p1.price < p2.price) {
        return -1;
    }
    return 0;
}

function sortByPrice(state, action) {
    return {
        ...state,
        items: [...state.items].sort((product1, product2) => {
            switch (action.payload.sortType) {
                case SORT_BY_PRICE_ASC:
                    return sort(product1, product2);
                case SORT_BY_PRICE_DESC:
                    return sort(product2, product1);
            }
        })
    };
}

function findProduct(state, action) {
    return {
      ...state,
        searchBy: action.payload.subString
    };
}

function filterProducts(state, action) {
    const filterId = action.payload.filterId;
    if (state.filterBy.includes(filterId)) {
        return {
            ...state,
            filterBy: state.filterBy.filter(id => id !== filterId)
        }
    }

    return {
        ...state,
        filterBy: state.filterBy.concat(filterId)
    }
}

function filterProductsByPrice(state, action) {
    const min = action.payload.min;
    const max = action.payload.max;
    return {
        ...state,
        minPrice: min,
        maxPrice: max
    }
}

export default function productReducer(state = initialState, action) {
    switch(action.type) {
        case FETCH_PRODUCTS_BEGIN:
            return startLoadingProducts(state);
        case FETCH_PRODUCTS_SUCCESS:
            return getLoadedProducts(state, action);
        case FETCH_PRODUCTS_FAILURE:
            return getErrorFromLoadingProducts(state, action);
        case ADD_PRODUCT:
            return addProduct(state, action);
        case DELETE_PRODUCT:
            return deleteProduct(state, action);
        case SORT_BY_PRICE:
            return sortByPrice(state, action);
        case FIND_PRODUCT:
            return findProduct(state, action);
        case FILTER_PRODUCTS:
            return filterProducts(state, action);
        case FILTER_PRODUCTS_BY_PRICE:
            return filterProductsByPrice(state, action);
        default:
            return state;
    }
};