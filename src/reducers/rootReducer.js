import { combineReducers } from "redux";
import products from "./productReducer";
import filters from "./filterReducer";

export default combineReducers({
    products: products,
    filters: filters
});