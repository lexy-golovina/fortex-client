import {
    ADD_FILTER,
    COLLAPSE_FILTER, DELETE_FILTER, FETCH_FILTERS_BEGIN, FETCH_FILTERS_FAILURE, FETCH_FILTERS_SUCCESS,
} from "../constants/actionTypes";

const initialState = {
    items: [],
    filterToCollapse: ''
};

function startLoadingFilters(state) {
    return {
        ...state,
        loading: true,
        error: null
    };
}

function getLoadedFilters(state, action) {
    return {
        ...state,
        loading: false,
        items: action.payload.filters.map(item => ({
            ...item,
            open: true
        }))
    };
}

function getErrorFromLoadingFilters(state, action) {
    return {
        ...state,
        loading: false,
        error: action.payload.error
    };
}

function collapse(item, id) {
    if (item.id === id) {
        return !item.open;
    }
    return item.open;
}

function collapseFilter(state, action) {
    return {
        ...state,
        items: state.items.map(item => ({
            ...item,
            open: collapse(item, action.payload.id)
        }))
    };
}

function addFilter(state, action) {
    return {
        ...state,
        items: state.items.concat(action.payload.filter)
    };
}

function deleteFilter(state, action) {
    return {
        ...state,
        items: state.items.filter(filter => filter.id !== action.payload.filterId)
    };
}

export default function filterReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_FILTERS_BEGIN:
            return startLoadingFilters(state);
        case FETCH_FILTERS_SUCCESS:
            return getLoadedFilters(state, action);
        case FETCH_FILTERS_FAILURE:
            return getErrorFromLoadingFilters(state, action);
        case COLLAPSE_FILTER:
            return collapseFilter(state, action);
        case ADD_FILTER:
            return addFilter(state, action);
        case DELETE_FILTER:
            return deleteFilter(state, action);
        default:
            return state;
    }
};