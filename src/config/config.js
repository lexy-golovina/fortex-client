const API_URL  = function host() {
    switch (process.env.NODE_ENV) {
        case 'dev':
            return 'http://localhost:8080';
        default:
            return 'https://fortex-server.herokuapp.com'
    }
}();

export {API_URL};